<?php

/*
    Plugin Name: BBB Register
    Plugin URI:
    Description: Add support to start/stop and download videos from the BBB instance of Merge-IT.
    Author: Daniele Mte90 Scasciafratte
    Version: 1.0.0
    Author URI:
*/

add_filter('manage_bbb-room_posts_columns', 'bbb_register_column');

function bbb_register_column($columns){
    $columns[ 'record' ] = __( 'Stato Registrazione' );
	return $columns;
}

add_action( 'manage_bbb-room_posts_custom_column' , 'bbb_register_record_column', 10, 2 );
	
function bbb_register_record_column( $column, $post_id ) {
	if ( $column === 'record' ) {
        if ( empty( get_option( 'bbb-recorder-status-' . get_the_ID() ) ) ) {
            $nonce = wp_create_nonce( 'bbb_register_start' );
            echo '<a href="edit.php?post_type=bbb-room&bbb-register=true&bbb-room=' . get_the_ID() . '&&bbb-nonce=' . esc_attr( $nonce ) . '">Avvia</a>';
        } else {
            $nonce = wp_create_nonce( 'bbb_register_stop' );
            echo '<a href="edit.php?post_type=bbb-room&bbb-stop=true&bbb-room=' . get_the_ID() . '&&bbb-nonce=' . esc_attr( $nonce ) . '">Ferma</a>';        
        }
	}
}

add_action( 'admin_init', 'bbb_register_endpoint' );

function bbb_register_endpoint() {
    if ( !\current_user_can( 'edit_bbb_rooms' ) ) {
        return;
    }
    
    if ( empty( $_GET[ 'bbb-register' ] ) ) {
        return;
    }
    
    if ( !wp_verify_nonce( \sanitize_text_field( \wp_unslash( $_GET[ 'bbb-nonce' ] ) ), 'bbb_register_start' ) ) {
        return;
    }
    
    $bbbroom = sanitize_text_field( \wp_unslash( $_GET[ 'bbb-room' ] ) );
    $bbbrom_slug = basename( get_permalink( $bbbroom ) );
    $bbb_id = get_post_meta( $bbbroom, 'bbb-room-meeting-id', true );
    
    update_option( 'bbb-recorder-status-' . $bbbroom, 'recording' );
    
    $request = wp_remote_get( 'http://eventi.merge-it.net:8081/?bbb_meeting=' . $bbbrom_slug . '&bbb_id=' . $bbb_id . '&bbb_add=true&bbb_secret=' . get_option( 'bigbluebutton_salt' ) );
    
    add_action( 'admin_notices', function() {
        $class = 'notice notice-success';
        printf( '<div class="%1$s"><p>La registrazione sarà avviata in un minuto!</p></div>', esc_attr( $class ) ); 
    } );
}

add_action( 'admin_init', 'bbb_stop_endpoint' );

function bbb_stop_endpoint() {
    if ( !\current_user_can( 'edit_bbb_rooms' ) ) {
        return;
    }
    
    if ( empty( $_GET[ 'bbb-stop' ] ) ) {
        return;
    }
    
    if ( !wp_verify_nonce( \sanitize_text_field( \wp_unslash( $_GET[ 'bbb-nonce' ] ) ), 'bbb_register_stop' ) ) {
        return;
    }
    
    $bbbroom = sanitize_text_field( \wp_unslash( $_GET[ 'bbb-room' ] ) );
    $bbbrom_slug = basename( get_permalink( $bbbroom ) );
    $bbb_id = get_post_meta( $bbbroom, 'bbb-room-meeting-id', true );
    
    $request = wp_remote_get( 'http://eventi.merge-it.net:8081/?bbb_meeting=' . $bbbrom_slug . '&bbb_id=' . $bbb_id . '&bbb_remove=true&bbb_secret=' . get_option( 'bigbluebutton_salt' ) );
    
    delete_option( 'bbb-recorder-status-' . $bbbroom, '' );
    
    add_action( 'admin_notices', function() {
        $class = 'notice notice-success';
        printf( '<div class="%1$s"><p>La registrazione è stata fermata!</p></div>', esc_attr( $class ) ); 
    } );
}

add_filter( 'the_content', 'bbb_add_download' );

function bbb_add_download( $content ) {
    if ( get_post_type() === 'bbb-room' ) {
        $bbbrom_slug = basename( get_permalink( get_the_ID() ) );
        $content .= '<br>Download Link: <a href="http://eventi.merge-it.net:8081/?bbb_meeting=' . $bbbrom_slug . '&bbb_recording=true">Click here</a>';
    }

    return $content;
} 

function add_bbb_download_box_markup() {
    if ( isset( $_GET[ 'post' ] ) ) {
        $bbbrom_slug = basename( get_permalink( $_GET[ 'post' ] ) );
        echo 'Download Link: <a href="http://eventi.merge-it.net:8081/?bbb_meeting=' . $bbbrom_slug . '&bbb_recording=true">Click here</a>';
    } else {
        echo 'Download Link: Not yet avalaible';    
    }
}
 
function add_bbb_download_box() {
    add_meta_box("bbb-download-box", "Download recording", "add_bbb_download_box_markup", "bbb-room", "side");
}
 
add_action("add_meta_boxes", "add_bbb_download_box");
