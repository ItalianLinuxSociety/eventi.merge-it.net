#!/usr/bin/env python3

# pip3 install pyyaml
from http.server import BaseHTTPRequestHandler, HTTPServer
import json
import yaml
import urllib
import os
import shutil
import sys


class HTTPServer_RequestHandler(BaseHTTPRequestHandler):
    def setup(self):
        BaseHTTPRequestHandler.setup(self)
        self.request.settimeout(180)

    def do_GET(self):
        parsed_path = urllib.parse.urlsplit(self.path)
        query = urllib.parse.parse_qs(parsed_path.query)
        self.send_response(200)
        message = ''
        bbb_meeting = ''
        if 'bbb_meeting' not in query:
            self.send_response(300)
            message = {'status': 'error', 'text': 'you are looking to a private web server! Go away!'}
        else:
            bbb_meeting = ' '.join(map(str, query['bbb_meeting']))

        dockercomposefolder = os.getcwd() + '/bbb-record-' + bbb_meeting
        dockercompose = dockercomposefolder + '/docker-compose.yml'
        video = dockercomposefolder + '/videodata/'

        if 'bbb_secret' in query:
            os.system('cd /root')
            if not os.path.exists(os.getcwd() + '/docker-bbb-base.yml'):
                print(os.getcwd() + '/docker-bbb-base.yml not exists!')
                sys.exit()

            with open(os.getcwd() + '/docker-bbb-base.yml') as f:
                data = yaml.safe_load(f)
                message = ''
                for item in data['services']['bbb-streamer']['environment']:
                    if item == 'BBB_SECRET=' + ' '.join(map(str, query['bbb_secret'])):
                        message = {'status': 'success', 'text': 'secret found'}
                        break

                if message == '':
                    self.send_response(300)
                    message = {'status': 'error', 'text': 'wrong secret'}

            if 'bbb_meeting' in query:
                bbb_id = ' '.join(map(str, query['bbb_id']))
                if 'bbb_add' in query:
                    fin = open(os.getcwd() + '/docker-bbb-base.yml', "rt")
                    message = {'status': 'success', 'text': 'meeting configured'}
                    if os.path.exists(dockercomposefolder):
                        shutil.rmtree(dockercomposefolder, ignore_errors=True)
                        message = {'status': 'success', 'text': 'meeting already exist, removed and created again'}
                    
                    os.mkdir(dockercomposefolder)
                    fout = open(dockercompose, "wt")

                    for line in fin:
                        fout.write(line.replace('your_meetingID', bbb_id).replace('containerID', bbb_meeting))

                    fin.close()
                    fout.close()
                    os.system('cd ' + dockercomposefolder + ' && docker-compose up --force-recreate --build -d &')

                    message = {'status': 'success', 'text': 'docker container started'}

                elif 'bbb_remove' in query:
                    if os.path.exists(dockercomposefolder):
                        os.system('cd ' + dockercomposefolder + ' && docker-compose down &')
                        #os.remove(dockercompose)

                    message = {'status': 'success', 'text': 'meeting recording stopped'}

        if 'bbb_recording' in query:
            if os.path.isdir(video):
                for i in os.listdir(video):
                    if os.path.isfile(os.path.join(video, i)) and 'record' in i:
                        self.send_header("Content-Type", 'application/octet-stream')
                        with open(os.path.join(video, i), 'rb') as f:
                            self.send_header("Content-Disposition", 'attachment; filename="{}"'.format(i))
                            fs = os.fstat(f.fileno())
                            self.send_header("Content-Length", str(fs.st_size))
                            self.end_headers()
                            try:
                                shutil.copyfileobj(f, self.wfile)
                            except:
                                print('Error on downloading ' + os.path.join(video, i))
                    else:
                        self.send_response(300)
                        message = {'status': 'error', 'text': 'recording not found'}
            else:
                message = {'status': 'success', 'text': 'recording not found, did you started it?'}

        if message != '':
            self.send_header('Content-type', 'text/json')
            try:
                self.end_headers()
            except BrokenPipeError:
                print('Browser blocked the download')
            self.wfile.write(bytes(json.dumps(message), "utf8"))

        return

    #def log_message(self, format, *args):
        #return

def run():
    print('Server started...')

    server_address = ('', 8081)
    httpd = HTTPServer(server_address, HTTPServer_RequestHandler)
    print('Server running...')
    httpd.serve_forever()


run() 
